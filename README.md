# Technical Test - Software Developer #

Spring boot microservice supporting the TODO api


### Quick Links? ###

* Health - http://localhost:8080/actuator/health
* API - http://localhost:8080/api-docs.yaml
* Swagger UI - http://localhost:8080/swagger-ui/index.html?configUrl=/api-docs/swagger-config
* h2 console - http://localhost:8080/h2-console

### Prerequisites ###

* Java - java version "15" 2020-09-15
* Internet connection (required for gradle & mvn repository)

### How do I get set up? ###

To build and run execute from the project directory
* ./gradlew bootRun

### Configuration ###

* The default server port can be changed from 8080 in application.properties

### Incomplete work ###
* Immutables instead of simple pojos
* Exception handling for invalid requests. Currently a simple error handler has been 
  implemented, however more information on what information is possible to return in an error message to avoid sensitive data escaping.
* More negative tests required on the api - including validating error messages
* validation to sync the API to the database implementation. e.g size of the name field
* Scenario: Idempotent create fails when request is different.
  This scenario has not been implemented as it requires some non-trivial changes to 
  spring data JPA. See comment in the todo.feature.

### Questions ###
* Due Date – Date field without a time - optional
  Is the "without a time component" important for the service? The service
  could accept a full date/time, and the "without a time" part then implemented by 
  the client, for example by truncating the time.

* Should the service enforce the state of the TODO? 
  For example upon creation it must be Pending. This may be a client only restriction
  allowing different clients to use the same API but with different constraints.
   
* Edit a todo – Don’t allow editing of the status field in the edit form
  Is this a feature for the service to enforce? As above, this could be a client only
  restriction.
  
* Mark a TODO as done. This could be implemented as a PATCH api call,
  For brevity's sake I have initially chosen to only implement the PUT api for now.  