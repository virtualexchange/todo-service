package com.example.todo.controller;

import com.example.todo.api.Todo;
import com.example.todo.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("todos")
public class TodoController {

    public static final Logger logger = LoggerFactory.getLogger(TodoController.class);

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public Set<Todo> retrieveAll() {
        return todoService.retrieveAll();
    }

    @GetMapping("/{name}")
    public Todo retrieveTodo(@PathVariable String name) {
        return todoService.retrieveTodo(name)
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found"
        ));
    }

    @DeleteMapping("/{name}")
    public void deleteTodo(@PathVariable String name) {
        todoService.deleteTodo(name);
    }

    @PutMapping("/{name}")
    public void updateTodo(@PathVariable String name, @Valid @RequestBody Todo todo) {
        if (!name.equals(todo.getName())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "malformed request");
        }
        todoService.updateTodo(todo);
    }

    @PostMapping
    public void createNewTodo(@Valid @RequestBody Todo todo) {
        logger.info("Creating new todo {}", todo);
        todoService.createNewTodo(todo);
    }

    /**
     * Custom exception handlers
     *  - More work may be required to structure error messages or hide sensitive data
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
        return new ResponseEntity<>("not valid due to validation error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> handleIllegalStateException(IllegalStateException e) {
        return new ResponseEntity<>("malformed request", HttpStatus.BAD_REQUEST);
    }

}
