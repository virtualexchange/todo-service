package com.example.todo.service;

import com.example.todo.api.Status;
import com.example.todo.api.Todo;
import com.example.todo.entity.TodoEntity;
import com.example.todo.repository.TodoRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * TODO The transactional annotation works for now, but will most likely require attention in the future.
 */
@Service
@Transactional
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    /**
     * TODO change to insert only functionality
     * then, any existing entity must be checked for equality or an exception thrown
     */
    public void createNewTodo(Todo todo) {
        todoRepository.save(toEntity(todo));
    }

    public Optional<Todo> retrieveTodo(String name) {
        return todoRepository.findById(name)
                .map(TodoService::toApi);
    }

    public Set<Todo> retrieveAll() {
        Iterator<TodoEntity> iterator = todoRepository.findAll().iterator();
        return Stream.generate(() -> null)
                .takeWhile(x -> iterator.hasNext())
                .map(n -> iterator.next())
                .map(TodoService::toApi)
                .collect(Collectors.toSet());
    }

    public void deleteTodo(String name) {
        if (todoRepository.existsById(name)) {
            todoRepository.deleteById(name);
        }
    }

    public void updateTodo(Todo todo) {
        if (todoRepository.existsById(todo.getName())) {
            todoRepository.save(toEntity(todo));
        } else {
            throw new IllegalStateException("Entity does not exist on update request");
        }
    }

    /**
     * TODO these methods need a home and unit tests
     */
    public static TodoEntity toEntity(Todo todo) {
        return new TodoEntity(todo.getName(), todo.getDescription(), todo.getDueDate(), todo.getStatus().name());
    }

    public static Todo toApi(TodoEntity todo) {
        return new Todo(todo.getName(), todo.getDescription(), todo.getDueDate(), Status.valueOf(todo.getStatus()));
    }

}
