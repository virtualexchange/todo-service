package com.example.todo.api;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;

/**
 * Simple implementation of the Todo object
 *
 * TODO Ideally this would use the Immutables framework, however time does not permit this...
 * TODO Entity and API objects have been merged due to compressed time
 */
public class Todo {

    @NotEmpty(message = "Name may not be empty")
    private final String name;
    @Size(max = 500, message = "Description must be less than 500 characters")
    private final String description;
    private final Date dueDate;
    @NotNull(message = "Status may not be empty")
    private final Status status;

    public Todo(String name, String description, Date dueDate, Status status) {
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return name.equals(todo.name) &&
                description.equals(todo.description) &&
                dueDate.equals(todo.dueDate) &&
                status == todo.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, dueDate, status);
    }
}
