package com.example.todo.api;

public enum Status {
    Pending,
    Done
}