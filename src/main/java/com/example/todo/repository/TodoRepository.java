package com.example.todo.repository;

import com.example.todo.api.Todo;
import com.example.todo.entity.TodoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TodoRepository extends CrudRepository<TodoEntity, String> {

    Optional<TodoEntity> findById(String name);

}
