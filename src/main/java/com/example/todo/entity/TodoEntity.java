package com.example.todo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;

/**
 * Simple implementation of the Todo object
 *
 * TODO Ideally this would use the Immutables framework, however time does not permit this...
 * TODO Entity and API objects have been merged due to compressed time
 */
@Entity
@Table(name = "TODOS")
public class TodoEntity {

    @Id
    private String name;
    private String description;
    private Date dueDate;
    private String status;

    public TodoEntity() {
    }

    public TodoEntity(String name) {
        this(name, null, null, null);
    }

    public TodoEntity(String name, String description, Date dueDate, String status) {
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
