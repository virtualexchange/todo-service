package com.example.todo;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sets required headers and optionally a body on the request executed by rest template
 */
public class HeaderSettingRequestCallback implements RequestCallback {

    private final String body;

    public HeaderSettingRequestCallback() {
        this(null);
    }

    public HeaderSettingRequestCallback(String body) {
        this.body = body;
    }

    @Override
    public void doWithRequest(ClientHttpRequest request) throws IOException {
        final HttpHeaders clientHeaders = request.getHeaders();
        clientHeaders.setContentType(MediaType.APPLICATION_JSON);
        clientHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        if (body != null) {
            request.getBody().write(body.getBytes());
        }
    }
}