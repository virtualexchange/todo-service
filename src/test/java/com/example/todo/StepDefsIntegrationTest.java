package com.example.todo;

import com.example.todo.api.Status;
import com.example.todo.api.Todo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class StepDefsIntegrationTest extends SpringIntegrationTest {

    private static final String FIRST_TODO = "firstTodo";
    private static final String FIRST_TODO_UPDATED = "firstTodoUpdated";
    private static final String SECOND_TODO = "secondTodo";
    private static final Map<String, Todo> TODOS = new HashMap<>() {{

        put(FIRST_TODO, new Todo(
                FIRST_TODO,
                "description",
                Date.from(ZonedDateTime.parse("2020-11-11T00:00:00Z").toInstant()),
                Status.Pending));
        put(FIRST_TODO_UPDATED, new Todo(
                FIRST_TODO,
                "descriptionUpdated",
                Date.from(ZonedDateTime.parse("2020-11-11T00:00:00Z").toInstant()),
                Status.Done));
        put(SECOND_TODO, new Todo(
                SECOND_TODO,
                "secondTodoDescription",
                Date.from(ZonedDateTime.parse("2020-11-12T00:00:00Z").toInstant()),
                Status.Pending));
    }};

    @When("client creates a new TODO without a name")
    public void clientCreatesANewTODOWithoutAName() throws Exception {
        Todo invalidTodo = new Todo(
                null,
                "description",
                Date.from(ZonedDateTime.parse("2020-11-11T00:00:00Z").toInstant()),
                Status.Pending);

        String body = objectMapper.writeValueAsString(invalidTodo);
        executePost("/todos", body);
    }

    @When("client creates a new TODO without a large description")
    public void clientCreatesANewTODOWithoutALargeDescription() throws Exception {
        String description501 = IntStream.range(0, 501).mapToObj(i -> "d").collect(Collectors.joining(""));
        Todo invalidTodo = new Todo(
                "invalidDescription",
                description501,
                Date.from(ZonedDateTime.parse("2020-11-11T00:00:00Z").toInstant()),
                Status.Pending);

        String body = objectMapper.writeValueAsString(invalidTodo);
        executePost("/todos", body);
    }


    @When("the client creates a new TODO")
    public void theClientCreatesANewTODO() throws Exception {
        String body = objectMapper.writeValueAsString(TODOS.get(FIRST_TODO));
        executePost("/todos", body);
    }

    @When("the client creates a second TODO")
    public void theClientCreatesASecondTODO() throws Exception {
        String body = objectMapper.writeValueAsString(TODOS.get(SECOND_TODO));
        executePost("/todos", body);
    }

    @When("the client retrieves previously created TODO")
    public void theClientRetrievesPreviouslyCreatedTODO() {
        executeGet("/todos/" + TODOS.get(FIRST_TODO).getName());
    }

    @Then("the client receives status code of {int}")
    public void the_client_receives_status_code_of(int statusCode) throws Exception {
        HttpStatus currentStatusCode = latestResponse.getClientHttpResponse().getStatusCode();
        assertThat("status code is incorrect : " + latestResponse.getBody(), currentStatusCode.value(), is(statusCode));
    }

    @Then("the retrieved TODO matches the previously created TODO")
    public void theRetrievedTODOMatchesThePreviouslyCreatedTODO() throws JsonProcessingException {
        String jsonBody = latestResponse.getBody();
        Todo retrievedTodo = objectMapper.readValue(jsonBody, Todo.class);
        assertThat("Retrieved TODO does not match created TODO" + jsonBody,
                retrievedTodo, is(TODOS.get(FIRST_TODO)));
    }

    @And("the retrieved TODOs match the previously created TODOs")
    public void theRetrievedTODOsMatchThePreviouslyCreatedTODOs() throws Exception {
        String jsonBody = latestResponse.getBody();
        Todo[] retrievedTodos = objectMapper.readValue(jsonBody, Todo[].class);
        assertThat("Expected 2 TODOS. Found: " + jsonBody,
                retrievedTodos.length, is(2));

        Arrays.stream(retrievedTodos)
                .forEach(retrievedTodo -> {
                    assertThat("Retrieved TODO does not match created TODO" + jsonBody,
                            retrievedTodo, is(TODOS.get(retrievedTodo.getName())));
                });
    }

    @When("the client retrieves a not existent TODO")
    public void theClientRetrievesANotExistentTODO() {
        executeGet("/todos/notExistent");
    }

    @When("the client retrieves all TODOs")
    public void theClientRetrievesAllTODOs() {
        executeGet("/todos");
    }

    @When("the client deletes second TODO")
    public void theClientDeletesSecondTODO() {
        executeDelete("/todos/" + TODOS.get(SECOND_TODO).getName());
    }

    @When("the client retrieves second TODO")
    public void theClientRetrievesSecondTODO() {
        executeGet("/todos/" + TODOS.get(SECOND_TODO).getName());
    }

    @When("the client updates second TODO")
    public void theClientUpdatesSecondTODO() throws Exception {
        String body = objectMapper.writeValueAsString(TODOS.get(SECOND_TODO));
        executePut("/todos/" + TODOS.get(SECOND_TODO).getName(), body);
    }

    @When("the client updates second TODO with malformed uri")
    public void theClientUpdatesSecondTODOWithMalformedUri() throws Exception {
        String body = objectMapper.writeValueAsString(TODOS.get(SECOND_TODO));
        executePut("/todos/nameNotMatchingObjectName", body);
    }

    @When("the client updates first TODO")
    public void theClientUpdatesFirstTODO() throws Exception {
        String body = objectMapper.writeValueAsString(TODOS.get(FIRST_TODO_UPDATED));
        executePut("/todos/" + TODOS.get(FIRST_TODO_UPDATED).getName(), body);
    }

    @When("the client retrieves updated TODO")
    public void theClientRetrievesUpdatedTODO() {
        executeGet("/todos/" + TODOS.get(FIRST_TODO_UPDATED).getName());
    }

    @And("the retrieved TODO matches the updated TODO")
    public void theRetrievedTODOMatchesTheUpdatedTODO() throws Exception {
        String jsonBody = latestResponse.getBody();
        Todo retrievedTodo = objectMapper.readValue(jsonBody, Todo.class);
        assertThat("Retrieved TODO does not match updated TODO" + jsonBody,
                retrievedTodo, is(TODOS.get(FIRST_TODO_UPDATED)));
    }

}