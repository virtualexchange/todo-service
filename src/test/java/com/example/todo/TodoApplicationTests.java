package com.example.todo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Simple integration tests that ensures that the application starts up
 */
@SpringBootTest
class TodoApplicationTests {

	@Test
	void contextLoadsWithoutErrors() {
	}

}
