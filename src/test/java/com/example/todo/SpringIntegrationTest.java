package com.example.todo;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@CucumberContextConfiguration
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SpringIntegrationTest {

    @Autowired
    protected RestTemplate restTemplate;

    @Autowired
    private ServletWebServerApplicationContext webServerAppCtxt;

    @Autowired
    protected ObjectMapper objectMapper;

    // Stores the latest response from the last executed rest call
    protected ResponseResults latestResponse = null;

    void executeGet(String url) {
        HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback();
        ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();

        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate.execute(convertToFullUri(url), HttpMethod.GET, requestCallback,
                responseExtractor(errorHandler));
    }

    void executeDelete(String url) {
        HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback();
        ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();

        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate.execute(convertToFullUri(url), HttpMethod.DELETE, requestCallback,
                responseExtractor(errorHandler));
    }

    void executePut(String uri, String body) {
        HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(body);
        ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();
        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate
          .execute(convertToFullUri(uri), HttpMethod.PUT, requestCallback, responseExtractor(errorHandler));
    }

    void executePost(String uri, String body) {
        HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(body);
        ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();
        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate
          .execute(convertToFullUri(uri), HttpMethod.POST, requestCallback, responseExtractor(errorHandler));
    }

    private ResponseExtractor<ResponseResults> responseExtractor(ResponseResultErrorHandler errorHandler) {
        return response -> {
            if (errorHandler.hadError) {
                return (errorHandler.getResults());
            } else {
                return (new ResponseResults(response));
            }
        };
    }

    /**
     * Converts an absolute path to a full URI including the random port the server is currently using
     * @param uri - The absolute path e.g /todos
     * @return full uri e.g http://localhost:7321/todos
     */
    private String convertToFullUri(String uri) {
        return "http://localhost:" + getPort() + uri;
    }

    private static class ResponseResultErrorHandler implements ResponseErrorHandler {
        private ResponseResults results = null;
        private Boolean hadError = false;

        private ResponseResults getResults() {
            return results;
        }

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            hadError = response.getRawStatusCode() >= 400;
            return hadError;
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
            results = new ResponseResults(response);
        }
    }

    private int getPort() {
        return webServerAppCtxt.getWebServer().getPort();
    }

}