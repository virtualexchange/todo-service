package com.example.todo;

import org.apache.commons.io.IOUtils;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * Encapsulates a response from the rest template execution
 */
public class ResponseResults {

    private final ClientHttpResponse clientHttpResponse;
    private final String body;

    ResponseResults(ClientHttpResponse response) throws IOException {
        this.clientHttpResponse = response;
        InputStream bodyInputStream = response.getBody();
        StringWriter stringWriter = new StringWriter();
        IOUtils.copy(bodyInputStream, stringWriter, StandardCharsets.UTF_8);
        this.body = stringWriter.toString();
    }

    public ClientHttpResponse getClientHttpResponse() {
        return clientHttpResponse;
    }

    public String getBody() {
        return body;
    }
}