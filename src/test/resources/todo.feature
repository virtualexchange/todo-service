Feature: TODOS can be created, modified and deleted

  Scenario: Name validation
    When client creates a new TODO without a name
    Then the client receives status code of 400

  Scenario: Description validation
    When client creates a new TODO without a large description
    Then the client receives status code of 400

  Scenario: client retrieves a not existent TODO
    When the client retrieves a not existent TODO
    Then the client receives status code of 404

#  TODO Further validation omitted for now...

  Scenario: client creates a new TODO
    When the client creates a new TODO
    Then the client receives status code of 200

  Scenario: Idempotent create
    When the client creates a new TODO
    Then the client receives status code of 200

# TODO This is not trivial to implement in spring data due to the save method
# TODO performing a select then insert/update
#
#  Scenario: Idempotent create fails when request is different
#    When the client creates a new TODO with a different description
#    Then the client receives status code of 400

  Scenario: client retrieves a previously created TODO
    When the client retrieves previously created TODO
    Then the client receives status code of 200
    And  the retrieved TODO matches the previously created TODO

  Scenario: client creates a second TODO
    When the client creates a second TODO
    Then the client receives status code of 200

  Scenario: client retrieves all TODOs
    When the client retrieves all TODOs
    Then the client receives status code of 200
    And  the retrieved TODOs match the previously created TODOs

  Scenario: client deletes second TODO
    When the client deletes second TODO
    Then the client receives status code of 200

  # GET is checked twice - just to ensure that it does not take two calls to delete to actually delete the entity
  Scenario: client retrieves TODO after delete
    When the client retrieves second TODO
    Then the client receives status code of 404

  Scenario: idempotent delete - it must be possible to execute delete again
    When the client deletes second TODO
    Then the client receives status code of 200

  Scenario: client retrieves TODO after delete
    When the client retrieves second TODO
    Then the client receives status code of 404

  Scenario: client updates second TODO with malformed uri
    When the client updates second TODO with malformed uri
    Then the client receives status code of 400

  Scenario: client updates second TODO after delete
    When the client updates second TODO
    Then the client receives status code of 400

  Scenario: client updates first TODO
    When the client updates first TODO
    Then the client receives status code of 200

  Scenario: client retrieves updated TODO
    When the client retrieves updated TODO
    Then the client receives status code of 200
    And  the retrieved TODO matches the updated TODO




